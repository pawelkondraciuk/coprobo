package pb.wi.coprobo.states;

import pb.wi.coprobo.Assets;
import pb.wi.coprobo.objects.Character;
import pb.wi.coprobo.objects.Character.Move;
import pb.wi.coprobo.objects.Character.VIEW_DIRECTION;

public class Standing extends CharacterState {

	public void doMoving(Character character, Move mv) {
		super.doMoving(character, mv);
		switch(mv) {
		case GO_LEFT:
		case GO_RIGHT:
			character.setState(new Walking());
			break;
		case JUMP:
			character.setState(new Jumping(character));
			break;
		default:
			break;
		}
	}

	@Override
	public void update(Character character, float deltaTime) {
		super.update(character, deltaTime);
		if(!character.isGrounded())
			character.setState(new Falling());
	}

	@Override
	public int getAnimationIndex(Character character) {
		if(character.getViewDirection() == VIEW_DIRECTION.LEFT)
			return Assets.CharacterAnimationIndex.STAND_LEFT;
		else
			return Assets.CharacterAnimationIndex.STAND_RIGHT;
	}
}
