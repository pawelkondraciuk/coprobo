package pb.wi.coprobo.states;

import pb.wi.coprobo.Assets;
import pb.wi.coprobo.objects.Character;
import pb.wi.coprobo.objects.Character.Move;
import pb.wi.coprobo.objects.Character.VIEW_DIRECTION;

public class Walking extends CharacterState {

	@Override
	public void doMoving(Character character, Move mv) {
		super.doMoving(character, mv);
		switch(mv) {
		case GO_LEFT:
			character.getAcceleration().x = -Character.ACCELERATION;
			break;
		case GO_RIGHT:
			character.getAcceleration().x = Character.ACCELERATION;
			break;
		case JUMP:
			character.setState(new Jumping(character));
			break;
		case GO_DOWN:
			break;
		case GO_UP:
			break;
		default:
			character.setState(new Standing());
			break;
		}
	}

	@Override
	public void update(Character character, float deltaTime) {
		super.update(character, deltaTime);
		if(!character.isGrounded())
			character.setState(new Falling());
	}

	@Override
	public int getAnimationIndex(Character character) {
		if(character.getViewDirection() == VIEW_DIRECTION.LEFT)
			return Assets.CharacterAnimationIndex.LEFT;
		else
			return Assets.CharacterAnimationIndex.RIGHT;
	}
}
