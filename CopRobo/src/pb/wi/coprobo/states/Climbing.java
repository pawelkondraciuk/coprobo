package pb.wi.coprobo.states;

import pb.wi.coprobo.Assets;
import pb.wi.coprobo.objects.Character;
import pb.wi.coprobo.objects.Character.Move;
import pb.wi.coprobo.objects.Character.VIEW_DIRECTION;

public class Climbing extends CharacterState {

	@Override
	public void doMoving(Character character, Move mv) {
		//super.doMoving(character, mv);
		character.getAcceleration().y = 0;
		character.getTerminalVelocity().y = Character.MAX_JUMP_SPEED/3;
		switch(mv) {
		case GO_LEFT:
			character.getAcceleration().x = -Character.ACCELERATION;
			character.setViewDirection(VIEW_DIRECTION.LEFT);
			break;
		case GO_RIGHT:
			character.getAcceleration().x = Character.ACCELERATION;
			character.setViewDirection(VIEW_DIRECTION.RIGHT);
			break;
		case JUMP:
			break;
		case GO_DOWN:
			character.getAcceleration().y = -Character.ACCELERATION/2;
			break;
		case GO_UP:
			character.getAcceleration().y = Character.ACCELERATION/2;
			break;
		case NONE:
			
			character.getVelocity().y = 0;
			break;
		default:
			break;
		}
	}

	@Override
	public void update(Character character, float deltaTime) {
		super.update(character, deltaTime);
		if(character.ladder == null) {
			character.setState(new Falling());
		}
		/*if(!character.isGrounded())
			character.setState(new Falling());*/
	}

	@Override
	public int getAnimationIndex(Character character) {
		if(character.getViewDirection() == VIEW_DIRECTION.LEFT)
			return Assets.CharacterAnimationIndex.CLIMBING_LEFT;
		else
			return Assets.CharacterAnimationIndex.CLIMBING_RIGHT;
	}
}
