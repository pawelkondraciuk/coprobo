package pb.wi.coprobo.states;

import pb.wi.coprobo.objects.Weapon;

public class FiredRecently extends WeaponState {
	float time = 1.0f; // 1s

	@Override
	public void update(Weapon weapon, float deltaTime) {
		time -= deltaTime;
		
		if(time < 0){
			if(weapon.getAmmoCount() == 0)
				weapon.setState(new OutOfAmmo());
			else
				weapon.setState(new ReadyToFire());
		}
	}
}
