package pb.wi.coprobo.states;

import pb.wi.coprobo.objects.Character;
import pb.wi.coprobo.objects.Character.Move;
import pb.wi.coprobo.objects.Ladder.LadderSegment;

public abstract class CharacterState {

	float stateTime = 0;
	
	public void doMoving(Character character, Move mv) {
		switch(mv) {
		case GO_LEFT:
			character.setViewDirection(Character.VIEW_DIRECTION.LEFT);
			break;
		case GO_RIGHT:
			character.setViewDirection(Character.VIEW_DIRECTION.RIGHT);
			break;
		case GO_DOWN:
			if(character.ladder != null) 
				character.setState(new Climbing());
			break;
		case GO_UP:
			if(character.ladder != null && character.ladder.getSegment() != LadderSegment.TOP) 
				character.setState(new Climbing());
			break;
		case JUMP:
			break;
		case NONE:
			break;
		default:
			break;
		}
	}
	
	public void update(Character character, float deltaTime) {
		stateTime += deltaTime;
	}

	public abstract int getAnimationIndex(Character character);
}
