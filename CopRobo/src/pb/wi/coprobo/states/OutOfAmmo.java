package pb.wi.coprobo.states;

import pb.wi.coprobo.objects.Weapon;

public class OutOfAmmo extends WeaponState {

	@Override
	public void insertAmmo(Weapon weapon) {
		if(weapon.getAmmoCount() > 0)
			weapon.setState(new ReadyToFire());
	}

}
