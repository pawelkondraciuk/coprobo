package pb.wi.coprobo.states;

import pb.wi.coprobo.objects.Weapon;

public class ReadyToFire extends WeaponState {

	@Override
	public void fire(Weapon weapon, int dmg) {
		weapon.setAmmoCount(weapon.getAmmoCount()-1);
		weapon.shoot(dmg);
		weapon.setState(new FiredRecently());
	}
}
