package pb.wi.coprobo.states;

import pb.wi.coprobo.Assets;
import pb.wi.coprobo.objects.Character;
import pb.wi.coprobo.objects.Character.Move;
import pb.wi.coprobo.objects.Character.VIEW_DIRECTION;

public class Falling extends CharacterState {

	public void doMoving(Character character, Move mv) {
		super.doMoving(character, mv);
		switch(mv) {
		case GO_LEFT:
			character.getAcceleration().x = -Character.ACCELERATION;
			break;
		case GO_RIGHT:
			character.getAcceleration().x = Character.ACCELERATION;
		case JUMP:
			break;
		default:
			break;
		}
	}

	@Override
	public void update(Character character, float deltaTime) {
		super.update(character, deltaTime);
		if(character.isGrounded())
			character.setState(new Standing());
	}

	@Override
	public int getAnimationIndex(Character character) {
		if(character.getViewDirection() == VIEW_DIRECTION.LEFT)
			return Assets.CharacterAnimationIndex.FALL_LEFT;
		else
			return Assets.CharacterAnimationIndex.FALL_RIGHT;
	}
}
