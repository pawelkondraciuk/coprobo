package pb.wi.coprobo.states;

import pb.wi.coprobo.objects.Weapon;

public abstract class WeaponState {
	
	public void fire(Weapon weapon, int dmg) { };
	
	public void insertAmmo(Weapon weapon) { };
	
	public void update(Weapon weapon, float deltaTime) { };
}
