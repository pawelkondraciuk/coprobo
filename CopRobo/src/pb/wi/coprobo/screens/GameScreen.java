package pb.wi.coprobo.screens;

import pb.wi.coprobo.Assets;
import pb.wi.coprobo.CopRoboGame;
import pb.wi.coprobo.controller.WorldController;
import pb.wi.coprobo.view.WorldRenderer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL10;

public class GameScreen extends AbstractScreen {
	
	WorldController controller;
	WorldRenderer renderer;
	
	boolean paused;
	
	public GameScreen(CopRoboGame game) {
		super(game);
		
		controller = WorldController.getInstance();
		controller.init();
		renderer = new WorldRenderer(controller);
		
		paused = false;
	}

	@Override
	public void render(float delta) {
		if(Gdx.input.isKeyPressed(Keys.ESCAPE) ||
				controller.getCurLevel() == WorldController.MAX_LEVEL) {
			game.setScreen(new MainMenuScreen(game));
		}
		
		if(!paused)
			controller.update(delta);
		Gdx.gl.glClearColor(Assets.getInstance().backgroundColor.x, Assets.getInstance().backgroundColor.y, Assets.getInstance().backgroundColor.z, 1.0f);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		renderer.render();
	}

	@Override
	public void resize(int width, int height) {
		renderer.resize(width, height);
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		paused = true;
		
	}

	@Override
	public void resume() {
		paused = false;
		
	}

	@Override
	public void dispose() {
		renderer.dispose();
		
	}
}
