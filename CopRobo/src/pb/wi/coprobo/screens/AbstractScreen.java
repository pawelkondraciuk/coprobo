package pb.wi.coprobo.screens;

import pb.wi.coprobo.CopRoboGame;

import com.badlogic.gdx.Screen;

public abstract class AbstractScreen implements Screen {
	
	CopRoboGame game;
	
	protected AbstractScreen(CopRoboGame game) {
		this.game = game;
	}

}
