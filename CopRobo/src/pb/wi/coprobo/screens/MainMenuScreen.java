package pb.wi.coprobo.screens;

import pb.wi.coprobo.CopRoboGame;
import pb.wi.coprobo.command.CloseGameCommand;
import pb.wi.coprobo.command.MenuItem;
import pb.wi.coprobo.command.SetGameScreenCommand;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureWrap;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;

public class MainMenuScreen extends AbstractScreen {

	Stage stage;
	Skin skin;
	TextButtonStyle textButtonStyle;
	Image backgroundImage;
	TextureAtlas atlas;
	Texture backgroundTexture;
	Table table;

	public MainMenuScreen(CopRoboGame game) {
		super(game);

		stage = new Stage();
		Gdx.input.setInputProcessor(stage);

		backgroundTexture = new Texture(Gdx.files.internal("Assets/menubg.png"));
		backgroundTexture.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);

		backgroundImage = new Image(backgroundTexture);
		backgroundImage.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

		stage.addActor(backgroundImage);

		table = new Table();
		table.setFillParent(true);

		atlas = new TextureAtlas(Gdx.files.internal("Assets/button.pack"));

		skin = new Skin(Gdx.files.internal("Assets/menuSkin.json"), atlas);

		MenuItem startGame = new MenuItem("Nowa gra", 300.0f, 50.0f, new SetGameScreenCommand(game), skin);
		MenuItem endGame = new MenuItem("Zako�cz", 300.0f, 50.0f, new CloseGameCommand(), skin);

		stage.addActor(table);
		table.add(startGame.getTextButton()).width(300).padBottom(5);
		table.row();
		table.add(endGame.getTextButton()).width(300);

	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
		stage.draw();
		Table.drawDebug(stage);
	}

	@Override
	public void resize(int width, int height) {
		stage.setViewport(width, height, true);
		backgroundImage.setSize(width, height);
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		stage.dispose();
		skin.dispose();
	}

}
