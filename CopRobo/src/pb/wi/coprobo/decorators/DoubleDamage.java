package pb.wi.coprobo.decorators;

import pb.wi.coprobo.objects.Player;
import pb.wi.coprobo.view.WorldRenderer;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class DoubleDamage extends PlayerDecorator {
	public DoubleDamage(Player player) {
		super(player);
	}

	@Override
	public void render(SpriteBatch batch) {
		batch.setShader(WorldRenderer.shaderMonochrome);
		WorldRenderer.shaderMonochrome.setUniformf("u_color", 1f, 0.3f, 0f, 1f);
		super.render(batch);
		batch.setShader(null);
	}
	
	@Override
	public float getTime() {
		return 10;
	}
	
	@Override
	public int getAttackPower() {
		return player.getAttackPower()*2;
	}

	@Override
	public void update(float deltaTime) {
		int power = player.getAttackPower();
		player.setAttackPower(getAttackPower());
		super.update(deltaTime);
		player.setAttackPower(power);
	}
	
	@Override
	public String toString() {
		return "Podwojne obrazenia";
	}
}
