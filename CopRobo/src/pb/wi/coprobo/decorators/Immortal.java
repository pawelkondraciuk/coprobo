package pb.wi.coprobo.decorators;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import pb.wi.coprobo.objects.Character;
import pb.wi.coprobo.objects.Player;
import pb.wi.coprobo.view.WorldRenderer;

public class Immortal extends PlayerDecorator {
	public Immortal(Player player) {
		super(player);
	}
	
	@Override
	public void render(SpriteBatch batch) {
		batch.setShader(WorldRenderer.shaderMonochrome);
		WorldRenderer.shaderMonochrome.setUniformf("u_color", 0.5f, 0.5f, 0.5f, 0.5f);
		super.render(batch);
		batch.setShader(null);
	}

	@Override
	public float getTime() {
		return 10;
	}
	
	@Override
	public void applyDamage(int dmg) {
		player.applyDamage(0);
	}

	@Override
	public String toString() {
		return "Niezniszczalnosc";
	}
}
