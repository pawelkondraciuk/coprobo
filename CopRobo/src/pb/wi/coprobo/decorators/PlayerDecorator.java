package pb.wi.coprobo.decorators;

import java.util.List;

import pb.wi.coprobo.controller.WorldController;
import pb.wi.coprobo.objects.Block;
import pb.wi.coprobo.objects.Bullet;
import pb.wi.coprobo.objects.DestructableBlock;
import pb.wi.coprobo.objects.Door;
import pb.wi.coprobo.objects.DoorCard;
import pb.wi.coprobo.objects.GameObject;
import pb.wi.coprobo.objects.HardOpponent;
import pb.wi.coprobo.objects.Ladder;
import pb.wi.coprobo.objects.NormalOpponent;
import pb.wi.coprobo.objects.Player;
import pb.wi.coprobo.objects.PowerUp;
import pb.wi.coprobo.states.CharacterState;
import pb.wi.coprobo.visitor.Visitor;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public abstract class PlayerDecorator extends Player {

	float stateTime = 0;
	Player player;

	public PlayerDecorator(Player player) {
		super(player.getPosition());
		this.player = player;
	}
	
	public Player getCharacter() {
		return player;
	}
	
	public abstract float getTime();

	@Override
	public void render(SpriteBatch batch) {
		player.render(batch);
	}

	@Override
	public Animation[] getAnimations() {
		return player.getAnimations();
	}

	@Override
	public void visit(Bullet bullet) {
		player.visit(bullet);
	}

	@Override
	public void visit(NormalOpponent opponent) {
		player.visit(opponent);
	}

	@Override
	public void visit(HardOpponent opponent) {
		player.visit(opponent);
	}

	@Override
	public void visit(Player player) {
		this.player.visit(player);
	}

	@Override
	public void visit(Door door) {
		player.visit(door);
	}

	@Override
	public void visit(DoorCard doorCard) {
		player.visit(doorCard);
	}

	@Override
	public void visit(Ladder ladder) {
		player.visit(ladder);
	}

	@Override
	public void visit(PowerUp powerUp) {
		super.visit(powerUp);
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}

	@Override
	public Vector2 getPosition() {
		return player.getPosition();
	}

	@Override
	public List<Rectangle> getBounds() {
		return player.getBounds();
	}

	@Override
	public void update(float deltaTime) {
		stateTime += deltaTime;
		if(getTime() < stateTime) {
			WorldController.getInstance().takeOffDecoration(player);
		}
		player.update(deltaTime);
	}

	@Override
	public void applyDamage(int dmg) {
		player.applyDamage(dmg);
	}

	@Override
	public void setState(CharacterState state) {

		player.setState(state);
	}

	@Override
	public void OnBulletFired(Bullet b) {

		player.OnBulletFired(b);
	}

	@Override
	public void OnBulletCollision(Bullet b) {

		player.OnBulletCollision(b);
	}

	@Override
	public void OnPowerUpTaken(PowerUp p) {

		player.OnPowerUpTaken(p);
	}

	@Override
	public int getHealth() {

		return player.getHealth();
	}

	@Override
	public int getAttackPower() {

		return player.getAttackPower();
	}

	@Override
	public VIEW_DIRECTION getViewDirection() {

		return player.getViewDirection();
	}

	@Override
	public void setViewDirection(VIEW_DIRECTION viewDirection) {

		player.setViewDirection(viewDirection);
	}

	@Override
	public void visit(Block block) {

		player.visit(block);
	}

	@Override
	public void visit(DestructableBlock block) {

		player.visit(block);
	}

	@Override
	public Vector2 getCorrectionVector(GameObject obj) {

		return player.getCorrectionVector(obj);
	}

	@Override
	public Vector2 getCorrectionVector(Rectangle src, Rectangle target) {

		return player.getCorrectionVector(src, target);
	}

	@Override
	public boolean isGrounded() {

		return player.isGrounded();
	}

	@Override
	public void setGrounded(boolean grounded) {

		player.setGrounded(grounded);
	}

	@Override
	public Vector2 getVelocity() {

		return player.getVelocity();
	}

	@Override
	public Vector2 getAcceleration() {

		return player.getAcceleration();
	}

	@Override
	public Vector2 getDimension() {
		return player.getDimension();
	}

	@Override
	public void leftPressed() {

		player.leftPressed();
	}

	@Override
	public void rightPressed() {

		player.rightPressed();
	}

	@Override
	public void jumpPressed() {

		player.jumpPressed();
	}

	@Override
	public void firePressed() {

		player.firePressed();
	}

	@Override
	public void leftReleased() {

		player.leftReleased();
	}

	@Override
	public void rightReleased() {

		player.rightReleased();
	}

	@Override
	public void jumpReleased() {

		player.jumpReleased();
	}

	@Override
	public void fireReleased() {

		player.fireReleased();
	}
	
	@Override
	public int getAmmoCount() {
		return player.getAmmoCount();
	}
	
	@Override
	public float getPercentage() {
		return player.getPercentage();
	}

	@Override
	public void upPressed() {

		player.upPressed();
	}

	@Override
	public void downPressed() {

		player.downPressed();
	}

	@Override
	public void upReleased() {

		player.upReleased();
	}

	@Override
	public void downReleased() {

		player.downReleased();
	}

	@Override
	public int getCardCount() {

		return player.getCardCount();
	}

	@Override
	public void setAttackPower(int attackPower) {

		player.setAttackPower(attackPower);
	}

	@Override
	public int getMaxHealth() {

		return player.getMaxHealth();
	}

	@Override
	public Vector2 getTerminalVelocity() {

		return player.getTerminalVelocity();
	}
	
	
}
