package pb.wi.coprobo;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector3;

public class Assets {

	public class CharacterAnimationIndex {
		public static final int LEFT = 0;
		public static final int RIGHT = 1;
		public static final int UP = 2;
		public static final int JUMP_LEFT = 3;
		public static final int JUMP_RIGHT = 4;
		public static final int STAND_LEFT = 5;
		public static final int STAND_RIGHT = 6;
		public static final int FALL_LEFT = 7;
		public static final int FALL_RIGHT = 8;
		public static final int CLIMBING_LEFT = 9;
		public static final int CLIMBING_RIGHT = 10;
	}
	
	private static Assets instance = null;
	
	private static final float PLAYER_FRAME_DURATION = 0.21f;
	
	public static final float VIRTUAL_WIDTH = 13.0f;
	public static final float VIRTUAL_HEIGHT = 10.0f;
	
	public static final int PIXEL_DENSITY = 64;
	
	public Animation[] playerAnimations;
	public Animation[] playerFireAnimations;
	public Animation[] opponentAnimations;
	public Animation[] opponentFireAnimations;
	public TextureRegion floor;
	public TextureRegion leftCorner;
	public TextureRegion rightCorner;
	public TextureRegion leftWall;
	public TextureRegion rightWall;
	public TextureRegion centerWall;
	public TextureRegion destructableBlockFull;
	public TextureRegion destructableBlockHalf;
	public TextureRegion destructableBlockNone;
	public TextureRegion closedDoor;
	public TextureRegion openDoor;
	public TextureRegion doorCard;
	public TextureRegion bottomLadder;
	public TextureRegion middleLadder;
	public TextureRegion topLadder;
	public TextureRegion PowerUpCube;
	public TextureRegion finishBlock;
	
	public TextureRegion laserBeam;
	public Animation explosionAnimation;
	
	public Vector3 backgroundColor = new Vector3(0x64/255.0f, 0x95/255.0f, 0xed/255.0f);
	
	private Assets() {
		playerAnimations = new Animation[19];
		load();
	}
	
	public static Assets getInstance() {
		if(instance == null)
			instance = new Assets();
		return instance;
	}
	
	public void load() {
		Texture.setEnforcePotImages(false);
		
		loadTiles();
		loadExplosion();
		createPlayerAnimations();
		createOpponentAnimations();
	}
	
	private void loadTiles() {
		TextureRegion[][] tiles;
		tiles = TextureRegion.split(new Texture(Gdx.files.internal("Assets//tiles.png")), 64, 64);
		floor = tiles[9][1];
		leftCorner = tiles[8][1];
		rightCorner = tiles[8][0];
		leftWall = tiles[8][3];
		rightWall = tiles[8][2];
		centerWall = tiles[8][6];
		destructableBlockFull = tiles[3][0];
		destructableBlockHalf = tiles[3][2];
		destructableBlockNone = tiles[3][3];
		closedDoor = tiles[6][0];
		openDoor = tiles[6][1];
		doorCard = tiles[4][4];
		bottomLadder = tiles[7][1];
		middleLadder = tiles[7][0];
		topLadder = tiles[7][2];
		PowerUpCube = tiles[4][3];
		finishBlock = tiles[9][0];
	}
	
	public void loadExplosion() {
		TextureRegion[][] tiles = TextureRegion.split(new Texture(Gdx.files.internal("Assets//Sprites//explosion64_2.png")), 64, 64);
		laserBeam = tiles[0][0];
		explosionAnimation = new Animation(PLAYER_FRAME_DURATION, tiles[0][3], tiles[0][2], tiles[0][1]);
	}
	
	public void createPlayerAnimations() {
		TextureRegion[][] heroRegions = TextureRegion.split(new Texture(Gdx.files.internal("Assets//Sprites//heroV5_64.png")), 64, 64);
		
		playerAnimations = new Animation[11];
		playerAnimations[CharacterAnimationIndex.LEFT] = new Animation(PLAYER_FRAME_DURATION, heroRegions[0]); // LEFT
		playerAnimations[CharacterAnimationIndex.RIGHT] = new Animation(PLAYER_FRAME_DURATION, heroRegions[1]); // LEFT
		playerAnimations[CharacterAnimationIndex.UP] = new Animation(PLAYER_FRAME_DURATION, heroRegions[5]); // UP
		playerAnimations[CharacterAnimationIndex.JUMP_LEFT] = new Animation(PLAYER_FRAME_DURATION, heroRegions[13]); // JUMP_LEFT
		playerAnimations[CharacterAnimationIndex.JUMP_RIGHT] = new Animation(PLAYER_FRAME_DURATION, heroRegions[14]); // JUMP_RIGHT
		playerAnimations[CharacterAnimationIndex.STAND_LEFT] = new Animation(PLAYER_FRAME_DURATION, heroRegions[0][3]); // LEFT
		playerAnimations[CharacterAnimationIndex.STAND_RIGHT] = new Animation(PLAYER_FRAME_DURATION, heroRegions[1][0]); // LEFT
		playerAnimations[CharacterAnimationIndex.FALL_LEFT] = new Animation(PLAYER_FRAME_DURATION, heroRegions[0][2]); // LEFT
		playerAnimations[CharacterAnimationIndex.FALL_RIGHT] = new Animation(PLAYER_FRAME_DURATION, heroRegions[1][1]); // LEFT
		playerAnimations[CharacterAnimationIndex.CLIMBING_LEFT] = new Animation(PLAYER_FRAME_DURATION, heroRegions[6]); // LEFT
		playerAnimations[CharacterAnimationIndex.CLIMBING_RIGHT] = playerAnimations[CharacterAnimationIndex.CLIMBING_LEFT];
		
		playerFireAnimations = new Animation[11];
		playerFireAnimations[CharacterAnimationIndex.LEFT] = new Animation(PLAYER_FRAME_DURATION, heroRegions[2]); // LEFT
		playerFireAnimations[CharacterAnimationIndex.RIGHT] = new Animation(PLAYER_FRAME_DURATION, heroRegions[3]); // LEFT
		playerFireAnimations[CharacterAnimationIndex.UP] = new Animation(PLAYER_FRAME_DURATION, heroRegions[5]); // UP
		playerFireAnimations[CharacterAnimationIndex.JUMP_LEFT] = new Animation(PLAYER_FRAME_DURATION, heroRegions[15]); // JUMP_LEFT
		playerFireAnimations[CharacterAnimationIndex.JUMP_RIGHT] = new Animation(PLAYER_FRAME_DURATION, heroRegions[16]); // JUMP_RIGHT
		playerFireAnimations[CharacterAnimationIndex.STAND_LEFT] = new Animation(PLAYER_FRAME_DURATION, heroRegions[2][1]); // LEFT
		playerFireAnimations[CharacterAnimationIndex.STAND_RIGHT] = new Animation(PLAYER_FRAME_DURATION, heroRegions[3][2]); // LEFT
		playerFireAnimations[CharacterAnimationIndex.FALL_LEFT] = playerFireAnimations[CharacterAnimationIndex.LEFT]; // LEFT
		playerFireAnimations[CharacterAnimationIndex.FALL_RIGHT] = playerFireAnimations[CharacterAnimationIndex.RIGHT]; // LEFT
		playerFireAnimations[CharacterAnimationIndex.CLIMBING_LEFT] = new Animation(PLAYER_FRAME_DURATION, heroRegions[4]); // LEFT
		playerFireAnimations[CharacterAnimationIndex.CLIMBING_RIGHT] = new Animation(PLAYER_FRAME_DURATION, heroRegions[5]);
	}
	
	public void createOpponentAnimations() {
		TextureRegion[][] heroRegions = TextureRegion.split(new Texture(Gdx.files.internal("Assets//Sprites//guardBlue64.png")), 64, 64);
		
		opponentAnimations = new Animation[11];
		opponentAnimations[CharacterAnimationIndex.LEFT] = new Animation(PLAYER_FRAME_DURATION, heroRegions[0]); // LEFT
		opponentAnimations[CharacterAnimationIndex.RIGHT] = new Animation(PLAYER_FRAME_DURATION, heroRegions[1]); // LEFT
		opponentAnimations[CharacterAnimationIndex.UP] = new Animation(PLAYER_FRAME_DURATION, heroRegions[3]); // UP
		opponentAnimations[CharacterAnimationIndex.JUMP_LEFT] = new Animation(PLAYER_FRAME_DURATION, heroRegions[4]); // JUMP_LEFT
		opponentAnimations[CharacterAnimationIndex.JUMP_RIGHT] = new Animation(PLAYER_FRAME_DURATION, heroRegions[5]); // JUMP_RIGHT
		opponentAnimations[CharacterAnimationIndex.STAND_LEFT] = new Animation(PLAYER_FRAME_DURATION, heroRegions[0][3]); // LEFT
		opponentAnimations[CharacterAnimationIndex.STAND_RIGHT] = new Animation(PLAYER_FRAME_DURATION, heroRegions[1][0]); // LEFT
		opponentAnimations[CharacterAnimationIndex.FALL_LEFT] = new Animation(PLAYER_FRAME_DURATION, heroRegions[0][2]); // LEFT
		opponentAnimations[CharacterAnimationIndex.FALL_RIGHT] = new Animation(PLAYER_FRAME_DURATION, heroRegions[1][1]); // LEFT
		opponentAnimations[CharacterAnimationIndex.CLIMBING_LEFT] = new Animation(PLAYER_FRAME_DURATION, heroRegions[3]); // LEFT
		opponentAnimations[CharacterAnimationIndex.CLIMBING_RIGHT] = opponentAnimations[CharacterAnimationIndex.CLIMBING_LEFT];
		
		opponentFireAnimations = new Animation[11];
		opponentFireAnimations[CharacterAnimationIndex.LEFT] = new Animation(PLAYER_FRAME_DURATION, heroRegions[9]); // LEFT
		opponentFireAnimations[CharacterAnimationIndex.RIGHT] = new Animation(PLAYER_FRAME_DURATION, heroRegions[10]); // LEFT
		opponentFireAnimations[CharacterAnimationIndex.UP] = opponentAnimations[CharacterAnimationIndex.UP]; // UP
		opponentFireAnimations[CharacterAnimationIndex.JUMP_LEFT] = opponentAnimations[CharacterAnimationIndex.JUMP_LEFT]; // JUMP_LEFT
		opponentFireAnimations[CharacterAnimationIndex.JUMP_RIGHT] = opponentAnimations[CharacterAnimationIndex.JUMP_RIGHT]; // JUMP_RIGHT
		opponentFireAnimations[CharacterAnimationIndex.STAND_LEFT] = new Animation(PLAYER_FRAME_DURATION, heroRegions[9][3]); // LEFT
		opponentFireAnimations[CharacterAnimationIndex.STAND_RIGHT] = new Animation(PLAYER_FRAME_DURATION, heroRegions[10][0]); // LEFT
		opponentFireAnimations[CharacterAnimationIndex.FALL_LEFT] = opponentFireAnimations[CharacterAnimationIndex.LEFT]; // LEFT
		opponentFireAnimations[CharacterAnimationIndex.FALL_RIGHT] = opponentFireAnimations[CharacterAnimationIndex.RIGHT]; // LEFT
		opponentFireAnimations[CharacterAnimationIndex.CLIMBING_LEFT] = opponentAnimations[CharacterAnimationIndex.CLIMBING_LEFT];
		opponentFireAnimations[CharacterAnimationIndex.CLIMBING_RIGHT] = opponentAnimations[CharacterAnimationIndex.CLIMBING_LEFT];
	}
}
