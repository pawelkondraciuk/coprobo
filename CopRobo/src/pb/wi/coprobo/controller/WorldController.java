package pb.wi.coprobo.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Observer;
import java.util.Timer;
import java.util.TimerTask;

import pb.wi.coprobo.Assets;
import pb.wi.coprobo.CameraHelper;
import pb.wi.coprobo.builder.LevelBuilderDirector;
import pb.wi.coprobo.builder.StandardLevelBuilder;
import pb.wi.coprobo.decorators.PlayerDecorator;
import pb.wi.coprobo.objects.Bullet;
import pb.wi.coprobo.objects.Character;
import pb.wi.coprobo.objects.DamageAnimation;
import pb.wi.coprobo.objects.Explosion;
import pb.wi.coprobo.objects.GameObject;
import pb.wi.coprobo.objects.Level;
import pb.wi.coprobo.objects.MoveableGameObject;
import pb.wi.coprobo.objects.Player;
import pb.wi.coprobo.objects.StaticGameObject;
import pb.wi.coprobo.objects.TimeOfDay;
import pb.wi.coprobo.objects.TimeOfDay.Time;
import pb.wi.coprobo.visitor.Element;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.Vector2;

public class WorldController 
	extends InputAdapter {
	
	static WorldController instance = null;
	
	CameraHelper cameraHelper;

	float stateTime = 0.0f;

	Level currentLevel;
	Player player;
	boolean hasPowerUp = false;
	float powerUpTime = 0;
	int curLevel = 0;
	public static final int MAX_LEVEL = 8;
	
	TimeOfDay timeOfDay;

	List<Character> removedCharacters;
	List<GameObject> collidable;
	List<Explosion> explosions;
	List<Bullet> bullets;
	List<Bullet> closeBullets;
	List<Bullet> removedBullets;
	List<DamageAnimation> damageAnimations;
	
	Timer timer = new Timer();
	
	int cameraIndex = 0;
	
	WorldController() {

	}
		
	public static WorldController getInstance() {
		if(instance == null)
			instance = new WorldController();
		return instance;
	}

	public void init() {
		Gdx.input.setInputProcessor(this);
		cameraHelper = new CameraHelper();
		
		collidable = new ArrayList<GameObject>();
		explosions = new ArrayList<Explosion>();
		bullets = new ArrayList<Bullet>();
		closeBullets = new ArrayList<Bullet>();
		removedBullets = new ArrayList<Bullet>();
		damageAnimations = new ArrayList<DamageAnimation>();
		removedCharacters = new ArrayList<Character>();
		
		curLevel = 0;

		loadNextLevel();
		
		timer.schedule(new TimerTask() {
			
			@Override
			public void run() {
				if (timeOfDay.isDay()) {
					timeOfDay.changeCurrentTime(Time.NIGHT);
					Assets.getInstance().backgroundColor.set(0x34/255.0f, 0x35/255.0f, 0xed/255.0f);
				} else {
					timeOfDay.changeCurrentTime(Time.DAY);
					Assets.getInstance().backgroundColor.set(0x64/255.0f, 0x95/255.0f, 0xed/255.0f);
				}
			}
		}, 30000, 30000);
		
	}
	
	public void loadNextLevel() {
		curLevel++;
		if(curLevel >= MAX_LEVEL)
			return;
		loadLevel();
	}
	
	private void loadLevel() {
		// Budowanie poziomu
		String fileName = String.format("levels/level%s.txt", Integer.toString(curLevel));
		FileHandle levelFile = Gdx.files.internal(fileName);

		buildLevel(levelFile);

		timeOfDay = new TimeOfDay();
		
		for (pb.wi.coprobo.objects.Character character : currentLevel.getCharacterList()) {
			timeOfDay.addObserver((Observer) character);
		}
		
		currentLevel.getCharacterList().add(player = new Player(new Vector2(currentLevel.getStartPosition())));

		cameraHelper.setTarget(player);
		cameraHelper.fastUpdate();
	}

	private void buildLevel(FileHandle levelFile) {
		StandardLevelBuilder levelBuilder = new StandardLevelBuilder();
		LevelBuilderDirector levelBuilderDirector = new LevelBuilderDirector(
				levelBuilder);
		levelBuilderDirector.createLevel(levelFile);

		currentLevel = levelBuilder.getResult();
	}

	public CameraHelper getCameraHelper() {
		return cameraHelper;
	}

	public void update(float deltaTime) {
		cameraHelper.update(deltaTime);
		
		if(hasPowerUp) {
			powerUpTime -= deltaTime;
		}
		
		// Update level (static objects)
		for(StaticGameObject obj : currentLevel) {
			if(obj != null)
				obj.update(deltaTime);
		}
		
		for(StaticGameObject obj : currentLevel.getCollectable()) {
			obj.update(deltaTime);
		}
		
		for (DamageAnimation d : damageAnimations) {
			d.increasePosition();
		}
		
 		// Update and delete explosions
		Iterator<Explosion> e = explosions.iterator();
		while(e.hasNext()) {
			Explosion exp = e.next();
			
			if(exp.isExplosionFinished()) {
				e.remove();
			}
			
			exp.update(deltaTime);
		}
		
		// Update currentLevel.getCharacterList()
		for (pb.wi.coprobo.objects.Character obj : currentLevel.getCharacterList()) {
			
			obj.update(deltaTime);
			
			Vector2 position = obj.getPosition();
			Vector2 dimension = obj.getDimension();
			
			populateCollidableBlocks(position.x, position.y, dimension.x, dimension.y);
			collidable.addAll(currentLevel.getCollectable());

			for(GameObject collisionWith : collidable) {
				if(obj.intersects(collisionWith)) {
					((Element)collisionWith).accept(obj);
				}
			}
			
			for (pb.wi.coprobo.objects.Character c : currentLevel.getCharacterList()) {
				if(obj.intersects(c)) {
					c.accept(obj);
				}
			}
		}
		
		// Update bullets
		float wX = (float) (cameraHelper.getPosition().x - Math.ceil(Assets.VIRTUAL_WIDTH / 2) - 1);
		float wWidth = (float) (cameraHelper.getPosition().x + Math.ceil(Assets.VIRTUAL_WIDTH / 2) + 1);
		
		removedBullets.clear();
		
		for(Bullet b : bullets) {
			Vector2 position = b.getPosition();
			Vector2 dimension = b.getDimension();
			if(position.x >= wWidth || position.x + dimension.x < wX) {
				removedBullets.add(b);
				continue;
			}
			
			b.update(deltaTime);
			
			populateCollidableBlocks(position.x, position.y, dimension.x, dimension.y);
			
			for(GameObject collisionWith : collidable) {
				if(b.intersects(collisionWith)) {
					((Element)collisionWith).accept(b);
					break;
				}
			}
			
			for(MoveableGameObject collisionWith : currentLevel.getCharacterList()) {
				if(b.intersects(collisionWith)) {
					collisionWith.accept(b);
				}
			}
			
		}
		bullets.removeAll(removedBullets);
		
		// Update damage points
		Iterator<DamageAnimation> iterator = damageAnimations.iterator();
		while(iterator.hasNext()) {
			DamageAnimation d = iterator.next();
			if(d.isAniomationEnd())
				iterator.remove();
			else
				d.update(deltaTime);
		}
		
		for(Bullet b : closeBullets) {
			for(MoveableGameObject collisionWith : currentLevel.getCharacterList()) {
				if(b.intersects(collisionWith)) {
					collisionWith.accept(b);
				}
			}

		}
		
		closeBullets.clear();
		currentLevel.getCharacterList().removeAll(removedCharacters);
	}

	private void populateCollidableBlocks(float rX, float rY, float rWidth, float rHeight) {
		collidable.clear();
		
		int startX = (int) Math.floor(rX);
		int startY = (int) Math.floor(rY);
		int endX = (int) Math.floor(rX + rWidth);
		int endY = (int) Math.floor(rY + rHeight);
		
		for (int x = startX - 1; x <= endX + 1; x++) {
			for (int y = startY - 1; y <= endY + 1; y++) {
				if (x >= 0 && x < currentLevel.getWidth() && y >=0 && y < currentLevel.getHeight()) {
					GameObject obj = currentLevel.getStaticGameObject(x, y);
					if (obj != null)
						collidable.add(obj);
				}
			}
		}
	}

	@Override
	public boolean keyDown(int keycode) {
		if (keycode == Keys.LEFT)
			player.leftPressed();
		if (keycode == Keys.RIGHT)
			player.rightPressed();
		if (keycode == Keys.Z)
			player.jumpPressed();
		if (keycode == Keys.X)
			player.firePressed();
		if (keycode == Keys.UP)
			player.upPressed();
		if (keycode == Keys.DOWN)
			player.downPressed();
		return true;
	}

	@Override
	public boolean keyUp(int keycode) {
		if (keycode == Keys.LEFT)
			player.leftReleased();
		if (keycode == Keys.RIGHT)
			player.rightReleased();
		if (keycode == Keys.Z)
			player.jumpReleased();
		if (keycode == Keys.X)
			player.fireReleased();
		if (keycode == Keys.UP)
			player.upReleased();
		if (keycode == Keys.DOWN)
			player.downReleased();
		if (keycode == Keys.TAB)
			cameraHelper.setTarget(currentLevel.getCharacterList().get(++cameraIndex % currentLevel.getCharacterList().size()));
		return true;
	}

	public void onBulletFired(Character character, Bullet b) {
		bullets.add(b);
	}

	public void onBulletHit(Bullet b) {
		removedBullets.add(b);
	}
	
	public void onCloseWeaponAttack(Bullet b) {
		closeBullets.add(b);
	}

	public void onCollectableTaken(StaticGameObject p) {
		currentLevel.getCollectable().remove(p);
	}

	public void decoratePlayer(PlayerDecorator player) {
		hasPowerUp = true;
		powerUpTime = player.getTime();
		currentLevel.getCharacterList().remove(this.player);
		this.player = player;
		currentLevel.getCharacterList().add(player);
	}

	// koniec powerupa
	public void takeOffDecoration(Player player) {
		hasPowerUp = false;
		currentLevel.getCharacterList().remove(this.player);
		this.player = player;
		currentLevel.getCharacterList().add(player);
	}

	public void onDamageDealt(Character character, int dmg) {
		DamageAnimation damageAnimation = new DamageAnimation(dmg, character.getPosition().x, character.getPosition().y);
		damageAnimations.add(damageAnimation);
	}
	
	public void onCharacterDeath(Character character) {
		if(character == player) {
			loadLevel();
		} else {
			removedCharacters.add(character);
			explosions.add(new Explosion(character.getPosition()));
		}
	}
	
	public void onLevelEnd() {
		loadNextLevel();
	}

	public List<pb.wi.coprobo.objects.Character> getCharacterList() {
		return currentLevel.getCharacterList();
	}

	public List<Explosion> getExplosions() {
		return explosions;
	}

	public List<Bullet> getBullets() {
		return bullets;
	}

	public List<StaticGameObject> getCollectable() {
		return currentLevel.getCollectable();
	}

	public Level getCurrentLevel() {
		return currentLevel;
	}
	
	public List<DamageAnimation> getDamageAnimations() {
		return damageAnimations;
	}

	public boolean hasPowerUp() {
		return hasPowerUp;
	}

	public float getPowerUpTime() {
		return powerUpTime;
	}
	
	public Player getPlayer() {
		return player;
	}

	public int getCurLevel() {
		return curLevel;
	}
	
	
}
