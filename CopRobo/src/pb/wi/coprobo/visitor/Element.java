package pb.wi.coprobo.visitor;

public interface Element {
	public void accept(Visitor visitor);
}
