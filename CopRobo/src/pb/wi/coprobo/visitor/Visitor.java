package pb.wi.coprobo.visitor;

import pb.wi.coprobo.objects.Block;
import pb.wi.coprobo.objects.Bullet;
import pb.wi.coprobo.objects.DestructableBlock;
import pb.wi.coprobo.objects.Door;
import pb.wi.coprobo.objects.DoorCard;
import pb.wi.coprobo.objects.FinishBlock;
import pb.wi.coprobo.objects.HardOpponent;
import pb.wi.coprobo.objects.Ladder;
import pb.wi.coprobo.objects.NormalOpponent;
import pb.wi.coprobo.objects.Player;
import pb.wi.coprobo.objects.PowerUp;

public interface Visitor {
	public void visit(Block block);
	public void visit(Bullet bullet);
	public void visit(NormalOpponent opponent);
	public void visit(HardOpponent opponent);
	public void visit(Player player);
	public void visit(DestructableBlock block);
	public void visit(Door door);
	public void visit(DoorCard doorCard);
	public void visit(Ladder ladder);
	public void visit(PowerUp powerUp);
	public void visit(FinishBlock finishBlock);
}
