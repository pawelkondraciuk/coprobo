package pb.wi.coprobo.objects;

import pb.wi.coprobo.Assets;
import pb.wi.coprobo.visitor.Visitor;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Ladder extends StaticGameObject {
	
	public enum LadderSegment { TOP, MIDDLE, BOTTOM };

	LadderSegment segment;
	
	TextureRegion texture;
	
	public Ladder(Vector2 position, LadderSegment segment) {
		super(position);
		this.segment = segment;
		
		switch(segment) {
		case BOTTOM:
			texture = Assets.getInstance().bottomLadder;
			bounds.add(new Rectangle(0.2f, 0, 0.6f, 0.2f));
			break;
		case MIDDLE:
			texture = Assets.getInstance().middleLadder;
			bounds.add(new Rectangle(0.2f, 0, 0.6f, 1f));
			break;
		case TOP:
			texture = Assets.getInstance().topLadder;
			bounds.add(new Rectangle(0.2f, 0, 0.6f, 0.3f));
			break;
		default:
			break;
		}
		
		
	}

	@Override
	public void render(SpriteBatch batch) {
		batch.draw(texture.getTexture(), position.x, position.y, origin.x, origin.y, dimension.x, dimension.y, scale.x, scale.y, rotation, texture.getRegionX(), texture.getRegionY(), texture.getRegionWidth(),
				texture.getRegionHeight(), false, false);
	}
	
	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}
	
	public LadderSegment getSegment() {
		return segment;
	}

}
