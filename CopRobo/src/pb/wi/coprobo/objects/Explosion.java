package pb.wi.coprobo.objects;

import pb.wi.coprobo.Assets;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class Explosion extends GameObject {
	
	Animation anim;
	
	public Explosion(Vector2 position) {
		super(position);
		
		this.anim = Assets.getInstance().explosionAnimation;
		
		dimension.set(1, 1);
	}

	@Override
	public void render(SpriteBatch batch) {
		TextureRegion texture = anim.getKeyFrame(stateTime, false);
		batch.draw(texture.getTexture(), position.x, position.y, origin.x, origin.y, dimension.x, dimension.y, scale.x, scale.y, rotation, texture.getRegionX(), texture.getRegionY(), texture.getRegionWidth(),
				texture.getRegionHeight(), false, false);
	}
	
	public boolean isExplosionFinished() {
		return anim.isAnimationFinished(stateTime);
	}

}
