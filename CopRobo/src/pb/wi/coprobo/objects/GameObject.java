package pb.wi.coprobo.objects;

import java.util.ArrayList;
import java.util.List;

import pb.wi.coprobo.visitor.Element;
import pb.wi.coprobo.visitor.Visitor;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public abstract class GameObject {
	protected Vector2 position;
	protected Vector2 dimension;
	Vector2 origin;
	Vector2 scale;
	float rotation;
	
	List<Rectangle> bounds;
	float stateTime;

	public GameObject (Vector2 position) {
		this.position = position;
		dimension = new Vector2(1, 1);
		// Center image on game object
		origin = new Vector2(dimension.x / 2, dimension.y / 2);
		scale = new Vector2(1, 1);
		rotation = 0;
		bounds = new ArrayList<Rectangle>();
	}
	
	
	public Vector2 getPosition() {
		return position;
	}

	public void update(float delta) {
		stateTime += delta;
	}

	public abstract void render(SpriteBatch batch);

	public boolean intersects(GameObject obj) {
		boolean result = false;
		
		for(Rectangle src : bounds) {
			for(Rectangle dest : obj.bounds){
				
				Rectangle srcBounds = new Rectangle(position.x + src.x, position.y + src.y, src.width, src.height);
				Rectangle destBounds = new Rectangle(obj.position.x + dest.x, obj.position.y + dest.y, dest.width, dest.height);

				if(srcBounds.overlaps(destBounds))
					return true;
			}
		}
		
		return result;
	}
	
	public boolean intersects(GameObject obj, Vector2 position) {
		boolean result = false;
		
		for(Rectangle src : bounds) {
			for(Rectangle dest : obj.bounds){
				
				Rectangle srcBounds = new Rectangle(position.x + src.x, position.y + src.y, src.width, src.height);
				Rectangle destBounds = new Rectangle(obj.position.x + dest.x, obj.position.y + dest.y, dest.width, dest.height);

				if(srcBounds.overlaps(destBounds))
					return true;
			}
		}
		
		return result;
	}
	
	public boolean intersects(Rectangle rect) {
		boolean result = false;
		
		for(Rectangle src : bounds) {
			Rectangle srcBounds = new Rectangle(position.x + src.x, position.y + src.y, src.width, src.height);
			
			if(srcBounds.overlaps(rect))
				return true;
		}
		
		return result;
	}
	
	protected Rectangle getIntersection(Rectangle src, Rectangle target) {
		Rectangle intersection = new Rectangle();
		intersection.x = Math.max(src.x, target.x);
        intersection.width = Math.min(src.x + src.width, target.x + target.width) - intersection.x;
        intersection.y = Math.max(src.y, target.y);
        intersection.height = Math.min(src.y + src.height, target.y + target.height) - intersection.y;
        
        return intersection;
	}
	
	boolean collides_by_axis(float o1pos, float o1size, float o2pos, float o2size) {
		return ((o1pos + o1size) > o2pos &&
		        (o2pos + o2size) > o1pos);
	}
	float get_overlap(float o1previous_pos, float o1pos, float o1size, float o2pos, float o2size) {
		if (o1previous_pos > o2pos) {
			return -((o2pos + o2size) - o1pos);
		} else {
			return (o1pos + o1size) - o2pos;
		}
	}

	public List<Rectangle> getBounds() {
		return bounds;
	}


	public Vector2 getDimension() {
		return dimension;
	}
}
