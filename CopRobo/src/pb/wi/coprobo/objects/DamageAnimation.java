package pb.wi.coprobo.objects;

public class DamageAnimation {
	private int damage;
	private float x;
	private float y;
	private float stateTime;
	private float time = 2;
	
	public DamageAnimation(int damage, float x, float y) {
		this.damage = damage;
		this.x = x;
		this.y = y;
	}
	
	public void increasePosition() {
		this.y += 0.01f;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public int getDamage() {
		return damage;
	}
	
	public void update(float deltaTime) {
		stateTime += deltaTime;
	}
	
	public boolean isAniomationEnd() {
		return stateTime > time;
	}
}
