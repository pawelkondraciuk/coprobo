package pb.wi.coprobo.objects;

import pb.wi.coprobo.visitor.Element;
import pb.wi.coprobo.visitor.Visitor;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public abstract class MoveableGameObject extends GameObject implements Visitor, Element {
	protected Vector2 velocity;
	protected Vector2 terminalVelocity;
	protected Vector2 friction;
	protected Vector2 acceleration;
	
	protected boolean grounded = false;
	
	MoveableGameObject(Vector2 position) {
		super(position);
		velocity = new Vector2();
		terminalVelocity = new Vector2(1, 1);
		friction = new Vector2();
		acceleration = new Vector2();
	}

	@Override
	public void update(float deltaTime) {
		super.update(deltaTime);
		
		acceleration.scl(deltaTime);
		
		if (velocity.x != 0) {
			// Apply friction
			if (velocity.x > 0) {
				velocity.x = Math.max(velocity.x - friction.x * deltaTime, 0);
			} else {
				velocity.x = Math.min(velocity.x + friction.x * deltaTime, 0);
			}
		}
		
		if (velocity.y != 0) {
			// Apply friction
			if (velocity.y > 0) {
				velocity.y = Math.max(velocity.y - friction.y * deltaTime, 0);
			} else {
				velocity.y = Math.min(velocity.y + friction.y * deltaTime, 0);
			}
		}

		velocity.add(acceleration.x, acceleration.y);
		
		// Don't exceed terminal velocity
		velocity.x = MathUtils.clamp(velocity.x, -terminalVelocity.x, terminalVelocity.x);
		velocity.y = MathUtils.clamp(velocity.y, -terminalVelocity.y, terminalVelocity.y);
		
		velocity.scl(deltaTime);
		position.add(velocity);
		velocity.div(deltaTime);
	}
	
	protected void collide(GameObject block) {
		Vector2 correction = getCorrectionVector(block);
		position.add(correction);
		if(correction.y != 0) {
			if(velocity.y < 0 && correction.y > 0) {
				grounded = true;
				velocity.y = 0;
			} else if (velocity.y > 0 && correction.y < 0) {
				velocity.y = 0;
			}
		}
		if(correction.x != 0) {
			velocity.x = 0;
		}
	}
	
	@Override
	public void visit(Block block) {
		collide(block);
	}
	
	@Override
	public void visit(FinishBlock block) {
		collide(block);
	}
	
	@Override
	public void visit(DestructableBlock block) {
		collide(block);
	}

	@Override
	public void visit(Door door) {
		collide(door);
	}
	
	@Override
	public void visit(Bullet bullet) { }

	@Override
	public void visit(NormalOpponent opponent) { }

	@Override
	public void visit(HardOpponent opponent) { }

	@Override
	public void visit(Player player) { }

	@Override
	public void visit(DoorCard doorCard) { }

	@Override
	public void visit(Ladder ladder) { }

	@Override
	public void visit(PowerUp powerUp) { }

	public Vector2 getCorrectionVector(GameObject obj) {
		Vector2 ret = new Vector2();
		if(obj != null){
			for(Rectangle src : bounds) {
				for(Rectangle dest : obj.bounds) {
					
					Rectangle srcBounds = new Rectangle(position.x + src.x + ret.x, position.y + src.y + ret.y, src.width, src.height);
					Rectangle destBounds = new Rectangle(obj.position.x + dest.x, obj.position.y + dest.y, dest.width, dest.height);

					if(srcBounds.overlaps(destBounds)) {
						ret.add(getCorrectionVector(srcBounds, destBounds));
					}
				}
			}
		}
		
		return ret;
	}
	
	public Vector2 getCorrectionVector(Rectangle src, Rectangle target) {
		Vector2 ret = new Vector2();
		
		Rectangle intersection = getIntersection(src, target);
		
		if(target.width <= target.height)
    		ret.x = intersection.width * (intersection.x == src.x ? 1 : -1);
    	else
    		ret.y = intersection.height * (intersection.y == src.y ? 1 : -1);
		
		return ret;
	}

	public boolean isGrounded() {
		return grounded;
	}

	public void setGrounded(boolean grounded) {
		this.grounded = grounded;
	}

	public Vector2 getVelocity() {
		return velocity;
	}

	public Vector2 getAcceleration() {
		return acceleration;
	}

	public Vector2 getTerminalVelocity() {
		return terminalVelocity;
	}
}
