package pb.wi.coprobo.objects;

import java.util.List;

import pb.wi.coprobo.Assets;
import pb.wi.coprobo.visitor.Element;
import pb.wi.coprobo.visitor.Visitor;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class FinishBlock extends StaticGameObject {
	
	TextureRegion texture;
	
	public FinishBlock(Vector2 position) {
		super(position);
		
		bounds.add(new Rectangle(0, 0, 1, 0.2f));
		this.texture = Assets.getInstance().finishBlock;
	}

	@Override
	public void render(SpriteBatch batch) {
		batch.draw(texture.getTexture(), position.x, position.y, origin.x, origin.y, dimension.x, dimension.y, scale.x, scale.y, rotation, texture.getRegionX(), texture.getRegionY(), texture.getRegionWidth(),
				texture.getRegionHeight(), false, false);
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}
}
