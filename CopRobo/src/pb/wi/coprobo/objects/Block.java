package pb.wi.coprobo.objects;

import java.util.List;

import pb.wi.coprobo.visitor.Element;
import pb.wi.coprobo.visitor.Visitor;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Block extends StaticGameObject {
	
	TextureRegion texture;
	
	public Block(Vector2 position, TextureRegion texture, List<Rectangle> bounds) {
		super(position);
		
		this.bounds = bounds;
		this.texture = texture;
	}

	@Override
	public void render(SpriteBatch batch) {
		batch.draw(texture.getTexture(), position.x, position.y, origin.x, origin.y, dimension.x, dimension.y, scale.x, scale.y, rotation, texture.getRegionX(), texture.getRegionY(), texture.getRegionWidth(),
				texture.getRegionHeight(), false, false);
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}
}
