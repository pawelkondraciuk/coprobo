package pb.wi.coprobo.objects;

import pb.wi.coprobo.Assets;
import pb.wi.coprobo.controller.WorldController;
import pb.wi.coprobo.objects.Door.DoorState;
import pb.wi.coprobo.visitor.Element;
import pb.wi.coprobo.visitor.Visitor;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Bullet extends MoveableGameObject {

	TextureRegion texture;
	int dmgPoints;
	Character source;
	
	public Bullet(Vector2 position, Character source, int dx , int dmgPoints) {
		super(position);
		
		// Set physics values
		terminalVelocity.set(10.0f, 0.f);
		velocity.set(10.0f * dx, 0.0f);
		friction.set(0f, 0.0f);

		bounds.add(new Rectangle(0.4f, 0.3f, 0.1f, 0.3f));
		
		texture = Assets.getInstance().laserBeam;
		
		this.dmgPoints = dmgPoints;
		this.source = source;
	}

	@Override
	public void render(SpriteBatch batch) {
		batch.draw(texture.getTexture(), position.x, position.y, origin.x, origin.y, dimension.x, dimension.y, scale.x, scale.y, rotation, texture.getRegionX(), texture.getRegionY(), texture.getRegionWidth(),
				texture.getRegionHeight(), false, false);
	}

	@Override
	public void update(float delta) {
		super.update(delta);
	}
	
	@Override
	public void visit(Block block) {
		WorldController.getInstance().onBulletHit(this);
	}


	@Override
	public void visit(NormalOpponent opponent) {
		if(!(source instanceof Opponent))
			visit((Character)opponent);
	}

	@Override
	public void visit(HardOpponent opponent) {
		if(!(source instanceof Opponent))
			visit((Character)opponent);
	}
	
	public void visit(Character character) {
		character.applyDamage(dmgPoints);
		WorldController.getInstance().onBulletHit(this);
	}

	@Override
	public void visit(Player player) {
		if(!(source instanceof Player))
			visit((Character)player);
	}

	@Override
	public void visit(DestructableBlock block) {
		block.applyDamage();
		WorldController.getInstance().onBulletHit(this);
	}

	@Override
	public void visit(Door door) {
		if(door.getState() == DoorState.CLOSE)
			WorldController.getInstance().onBulletHit(this);
	}

	@Override
	public void accept(Visitor visitor) { }
}
