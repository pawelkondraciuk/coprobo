package pb.wi.coprobo.objects;

import java.util.Observable;
import java.util.Observer;

import pb.wi.coprobo.Assets;
import pb.wi.coprobo.controller.WorldController;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.Vector2;

public abstract class Opponent extends Character implements Observer {

	public static final float ACCELERATION = 20f;

	private VIEW_DIRECTION direction = VIEW_DIRECTION.LEFT;

	Strategy strategy;

	boolean firing;

	Player player;

	Opponent(Vector2 position, boolean day) {
		super(position);
		strategy = day ? new DayStrategy() : new NightStrategy();

		terminalVelocity.x = terminalVelocity.x * 0.5f;
		
		health = 5;
	}

	@Override
	public void update(Observable o, Object arg) {
		TimeOfDay timeOfDay = (TimeOfDay) o;

		if (timeOfDay.isDay()) {
			strategy = new DayStrategy();
		} else {
			strategy = new NightStrategy();
		}
	}

	@Override
	public void update(float deltaTime) {

		acceleration.y = GRAVITY;
		terminalVelocity.y = MAX_JUMP_SPEED;

		if (player == null) {
			player = WorldController.getInstance().getPlayer();
		}

		strategy.findPlayer(player);

		super.update(deltaTime);
	}

	@Override
	public Animation[] getAnimations() {
		if (!firing)
			return Assets.getInstance().opponentAnimations;
		else
			return Assets.getInstance().opponentFireAnimations;
	}

	public class NightStrategy implements Strategy {
		int sight = 2;

		@Override
		public void findPlayer(Player player) {
			boolean chasing = false;
			if ((int)position.x == (int)player.position.x) {
				doMoving(Move.NONE);
			} else if (position.x + sight >= player.position.x
					&& (position.x < player.position.x)
					&& (position.y + sight >= player.position.y)) {
				doMoving(Move.GO_RIGHT);
				jumpIfPlayerAbove(player);
				chasing = true;
			} else if (position.x - sight <= player.position.x
					&& (position.x > player.position.x)
					&& (position.y + sight >= player.position.y)) {
				doMoving(Move.GO_LEFT);
				jumpIfPlayerAbove(player);
				chasing = true;
			} else {
				doMoving(Move.NONE);
			}
			
			if(chasing && (int)position.y == (int)player.position.y)
				weapon.pullTrigger(getAttackPower());
		}

		private void jumpIfPlayerAbove(Player player) {
			if (position.y + sight > player.position.y && position.y < player.position.y) {
				doMoving(Move.JUMP);
			}
		}
	}

	public class DayStrategy implements Strategy {

		int sight = 4;

		@Override
		public void findPlayer(Player player) {
			boolean chasing = false;
			if ((int)position.x == (int)player.position.x) {
				doMoving(Move.NONE);
			} else if (position.x + sight >= player.position.x
					&& (position.x < player.position.x)
					&& (position.y + sight >= player.position.y)) {
				doMoving(Move.GO_RIGHT);
				jumpIfPlayerAbove(player);
				chasing = true;
			} else if (position.x - sight <= player.position.x
					&& (position.x > player.position.x)
					&& (position.y + sight >= player.position.y)) {
				doMoving(Move.GO_LEFT);
				jumpIfPlayerAbove(player);
				chasing = true;
			} else {
				doMoving(Move.NONE);
			}
			
			if(chasing && (int)position.y == (int)player.position.y)
				weapon.pullTrigger(getAttackPower());
		}

		private void jumpIfPlayerAbove(Player player) {
			if (position.y + sight > player.position.y && position.y < player.position.y) {
				doMoving(Move.JUMP);
			}
		}

	}

}
