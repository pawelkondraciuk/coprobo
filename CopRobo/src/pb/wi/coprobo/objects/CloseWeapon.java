package pb.wi.coprobo.objects;

import pb.wi.coprobo.controller.WorldController;
import pb.wi.coprobo.objects.Character.VIEW_DIRECTION;

import com.badlogic.gdx.math.Vector2;

public class CloseWeapon extends Weapon {

	public CloseWeapon(Character owner) {
		super(owner);
	}

	@Override
	public void shoot(int dmg) {
		WorldController.getInstance().onCloseWeaponAttack(new Bullet(position, owner, (direction == VIEW_DIRECTION.RIGHT ? 1 : -1), dmg));
	}
}
