package pb.wi.coprobo.objects;

import com.badlogic.gdx.math.Vector2;

import pb.wi.coprobo.controller.WorldController;
import pb.wi.coprobo.objects.Character.VIEW_DIRECTION;


public class RangedWeapon extends Weapon {

	public RangedWeapon(Character owner) {
		super(owner);
	}

	@Override
	public void shoot(int dmg) {
		Bullet b = new Bullet(position, owner, (direction == VIEW_DIRECTION.RIGHT ? 1 : -1), dmg);
		WorldController.getInstance().onBulletFired(owner, b);
	}
}
