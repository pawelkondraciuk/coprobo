package pb.wi.coprobo.objects;

import pb.wi.coprobo.objects.Character.VIEW_DIRECTION;
import pb.wi.coprobo.states.OutOfAmmo;
import pb.wi.coprobo.states.ReadyToFire;
import pb.wi.coprobo.states.WeaponState;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public abstract class Weapon extends GameObject {

	WeaponState state;

	int ammoCount = 500;
	
	protected VIEW_DIRECTION direction;
	
	Character owner;
	
	public Weapon(Character owner) {
		super(null);

		this.owner = owner;
		
		state = new ReadyToFire();
	}

	public void pullTrigger(int dmg) {
		state.fire(this, dmg);
	}
	
	public void insertAmmo(int count) {
		ammoCount += count;
		state.insertAmmo(this);
	}

	@Override
	public void update(float deltaTime) {
		state.update(this, deltaTime);
	}
	
	public void updatePosition(Character owner) {
		Vector2 pos = owner.getPosition().cpy();

		if(owner.viewDirection == VIEW_DIRECTION.LEFT) {
			pos.x -= 0.3f;
		} else {
			pos.x += 0.3f;//owner.dimension.x - 0.8f;
		}
		
		this.position = pos;
		this.direction = owner.viewDirection;
	}
	
	public void setState(WeaponState state) {
		this.state = state;
		System.out.println(state.toString());
	}

	@Override
	public void render(SpriteBatch batch) {	}
	
	public abstract void shoot(int dmg);

	public int getAmmoCount() {
		return ammoCount;
	}

	public void setAmmoCount(int ammoCount) {
		this.ammoCount = ammoCount;
	}
}
