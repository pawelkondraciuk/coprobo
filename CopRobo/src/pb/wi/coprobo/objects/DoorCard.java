package pb.wi.coprobo.objects;

import pb.wi.coprobo.Assets;
import pb.wi.coprobo.visitor.Visitor;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class DoorCard extends StaticGameObject {
	
	TextureRegion texture;
	Boolean used;

	public DoorCard(Vector2 position) {
		super(position);
		used = false;
		texture = Assets.getInstance().doorCard;
		bounds.add(new Rectangle(0.15f, 0.25f, 0.6f, 0.6f));
	}

	@Override
	public void render(SpriteBatch batch) {
		batch.draw(texture.getTexture(), position.x, position.y, origin.x, origin.y, dimension.x, dimension.y, scale.x, scale.y, rotation, texture.getRegionX(), texture.getRegionY(), texture.getRegionWidth(),
				texture.getRegionHeight(), false, false);
	}
	
	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}

	public Boolean getUsed() {
		return used;
	}

	public void setUsed(Boolean used) {
		this.used = used;
	}
}
