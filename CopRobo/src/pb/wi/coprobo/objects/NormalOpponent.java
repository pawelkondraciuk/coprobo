package pb.wi.coprobo.objects;

import java.util.Observable;

import pb.wi.coprobo.visitor.Visitor;

import com.badlogic.gdx.math.Vector2;

public class NormalOpponent extends Opponent {
	public NormalOpponent(Vector2 position, boolean day) {
		super(position, day);
		weapon = new CloseWeapon(this);
	}

	@Override
	public void visit(Player player) {
		weapon.pullTrigger(getAttackPower());
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}
}
