package pb.wi.coprobo.objects;

import java.util.Observable;

public class TimeOfDay extends Observable {

	private Time currentTime;

	public enum Time {
		DAY, NIGHT
	}

	public TimeOfDay() {
		currentTime = Time.DAY;
	}

	public boolean isDay() {
		if (currentTime == Time.DAY) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isNight() {
		if (currentTime == Time.NIGHT) {
			return true;
		} else {
			return false;
		}
	}

	public void changeCurrentTime(TimeOfDay.Time currentTime) {
		this.currentTime = currentTime;
		setChanged();
		notifyObservers();
	}

	public Time getCurrentTime() {
		return currentTime;
	}

}
