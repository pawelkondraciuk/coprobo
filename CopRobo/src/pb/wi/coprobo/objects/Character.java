package pb.wi.coprobo.objects;

import java.util.List;

import pb.wi.coprobo.controller.WorldController;
import pb.wi.coprobo.objects.Ladder.LadderSegment;
import pb.wi.coprobo.states.CharacterState;
import pb.wi.coprobo.states.Climbing;
import pb.wi.coprobo.states.Standing;
import pb.wi.coprobo.visitor.Visitor;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public abstract class Character extends MoveableGameObject {
	
	public enum Move {
		GO_LEFT, GO_RIGHT, GO_UP, GO_DOWN, JUMP, NONE
	};
	
	public enum VIEW_DIRECTION {
		LEFT, RIGHT
	};
	
	public static final float ACCELERATION 		= 40f;
	public static final float GRAVITY 			= -20f;
	public static final float MAX_JUMP_SPEED	= 9f;
	public static final float MAX_VEL 			= 5f;

	int health;
	int maxHealth;
	float percentage;
	protected int attackPower;
	CharacterState state;
	
	protected Weapon weapon;

	VIEW_DIRECTION viewDirection = VIEW_DIRECTION.LEFT;
	
	public Ladder ladder = null;
	
	public Character(Vector2 position) {
		super(position);

		// Set physics values
		terminalVelocity.set(MAX_VEL, MAX_JUMP_SPEED);
		friction.set(12.0f, 0.0f);
		acceleration.set(0.0f, GRAVITY);

		state = new Standing();
		
		bounds.add(new Rectangle(0.1f, 0, 0.8f, 0.8f));
		
		maxHealth = 10;
		health = maxHealth;
		
		percentage = health / maxHealth * 100;
		
		attackPower = 1;
	}

	@Override
	public void update(float deltaTime) {
		
		super.update(deltaTime);

		if (position.y < 0) {
			position.y = 0;
			grounded = true;
		}
		
		state.update(this, deltaTime);
		weapon.update(deltaTime);
		weapon.updatePosition(this);
		grounded = false;
		
		ladder = null;
	}
	
	@Override
	public void render(SpriteBatch batch) {
		Animation[] animations = getAnimations();
		
		TextureRegion reg = animations[state.getAnimationIndex(this)].getKeyFrame(stateTime, true);
		
		batch.draw(reg.getTexture(), position.x, position.y, origin.x, origin.y, dimension.x, dimension.y, scale.x, scale.y, rotation, reg.getRegionX(), reg.getRegionY(), reg.getRegionWidth(),
				reg.getRegionHeight(), false, false);
	}
	
	protected void doMoving(Move mv) {
		state.doMoving(this, mv);
	}
	
	public void applyDamage(int dmg) {
		health -= dmg;
		percentage = (float)health / maxHealth * 100;
		WorldController.getInstance().onDamageDealt(this, dmg);
		
		if(health <= 0)
			WorldController.getInstance().onCharacterDeath(this);
	}
	
	@Override
	public void visit(Ladder ladder) {
		this.ladder = ladder;
		
		List<Rectangle> bounds = ladder.getBounds();
		
		if(ladder.getSegment() == LadderSegment.MIDDLE)
			return;
		else if(ladder.getSegment() == LadderSegment.BOTTOM) {
			bounds.clear();
			bounds.add(new Rectangle(0.2f, 0, 0.6f,0.2f));
			collide(ladder);
			bounds.add(new Rectangle(0.2f, 0, 0.6f,1));
		} else {
			if(!(state instanceof Climbing)) {
				bounds.clear();
				bounds.add(new Rectangle(0.2f, 0, 0.6f, 0.2f));
				if(velocity.y <= 0)
					collide(ladder);
				bounds.add(new Rectangle(0.2f, 0, 0.6f, 0.3f));
			}
		}
		
		
	}

	public void setState(CharacterState state) {
		this.state = state;
		System.out.println(state);
	}

	public abstract Animation[] getAnimations();
	
	public void OnBulletFired(Bullet b) {
		WorldController.getInstance().onBulletFired(this, b);
	}
	
	public void OnBulletCollision(Bullet b) {
    	WorldController.getInstance().onBulletHit(b);
	}
	
	public void OnPowerUpTaken(PowerUp p) {
		WorldController.getInstance().onCollectableTaken(p);
	}

	public int getAttackPower() {
		return attackPower;
	}

	public VIEW_DIRECTION getViewDirection() {
		return viewDirection;
	}

	public void setViewDirection(VIEW_DIRECTION viewDirection) {
		this.viewDirection = viewDirection;
	}
	
	public int getAmmoCount() {
		return weapon.getAmmoCount();
	}
	
	public int getHealth() {
		return health;
	}
	
	public float getPercentage() {
		return percentage;
	}

	public void setAttackPower(int attackPower) {
		this.attackPower = attackPower;
	}

	public int getMaxHealth() {
		return maxHealth;
	}

	
}
