package pb.wi.coprobo.objects;

import java.util.Observable;

import pb.wi.coprobo.view.WorldRenderer;
import pb.wi.coprobo.visitor.Visitor;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class HardOpponent extends Opponent {
	public HardOpponent(Vector2 position, boolean day) {
		super(position, day);
		terminalVelocity.x = terminalVelocity.x * 0.3f;
		weapon = new RangedWeapon(this);
		health = 5;
	}
	
	@Override
	public void render(SpriteBatch batch) {
		batch.setShader(WorldRenderer.shaderMonochrome);
		WorldRenderer.shaderMonochrome.setUniformf("u_color", 1f, 0.3f, 0f, 1f);
		super.render(batch);
		batch.setShader(null);
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}
}
