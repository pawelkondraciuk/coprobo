package pb.wi.coprobo.objects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pb.wi.coprobo.Assets;
import pb.wi.coprobo.controller.WorldController;
import pb.wi.coprobo.visitor.Visitor;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.Vector2;

public class Player extends Character {
	
	List<DoorCard> cards = new ArrayList<DoorCard>();
	
	enum Keys {
		LEFT, RIGHT, UP, DOWN, JUMP, FIRE
	}
	static Map<Keys, Boolean> keys = new HashMap<Player.Keys, Boolean>();
	static {
		keys.put(Keys.LEFT, false);
		keys.put(Keys.RIGHT, false);
		keys.put(Keys.JUMP, false);
		keys.put(Keys.DOWN, false);
		keys.put(Keys.UP, false);
		keys.put(Keys.FIRE, false);
	};
	
	public Player(Vector2 position) {
		super(position);
		weapon = new RangedWeapon(this);
	}

	@Override
	public void update(float deltaTime) {

		acceleration.y = GRAVITY;
		terminalVelocity.y = MAX_JUMP_SPEED;

		processInput();

		super.update(deltaTime);
	}

	private void processInput() {
		if (keys.get(Keys.LEFT)) {
			doMoving(Move.GO_LEFT);
		} else if (keys.get(Keys.RIGHT)) {
			doMoving(Move.GO_RIGHT);
		}
		if (keys.get(Keys.UP)) {
			doMoving(Move.GO_UP);
		} else if (keys.get(Keys.DOWN)) {
			doMoving(Move.GO_DOWN);
		}
		if (keys.get(Keys.JUMP)) {
			doMoving(Move.JUMP);
		}
		if(!keys.get(Keys.LEFT) && !keys.get(Keys.RIGHT) && !keys.get(Keys.JUMP) && !keys.get(Keys.UP) && !keys.get(Keys.DOWN)) {
			doMoving(Move.NONE);
		}
		if(keys.get(Keys.FIRE))
			weapon.pullTrigger(getAttackPower());
	}
	
	public void leftPressed() {
		keys.get(keys.put(Keys.LEFT, true));
	}
	
	public void rightPressed() {
		keys.get(keys.put(Keys.RIGHT, true));
	}
	
	public void jumpPressed() {
		keys.get(keys.put(Keys.JUMP, true));
	}
	
	public void firePressed() {
		keys.get(keys.put(Keys.FIRE, true));
	}
	
	public void upPressed() {
		keys.get(keys.put(Keys.UP, true));
	}
	
	public void downPressed() {
		keys.get(keys.put(Keys.DOWN, true));
	}
	
	public void leftReleased() {
		keys.get(keys.put(Keys.LEFT, false));
	}
	
	public void rightReleased() {
		keys.get(keys.put(Keys.RIGHT, false));
	}
	
	public void jumpReleased() {
		keys.get(keys.put(Keys.JUMP, false));
	}
	
	public void fireReleased() {
		keys.get(keys.put(Keys.FIRE, false));
	}
	
	public void upReleased() {
		keys.get(keys.put(Keys.UP, false));
	}
	
	public void downReleased() {
		keys.get(keys.put(Keys.DOWN, false));
	}
	@Override
	public Animation[] getAnimations() {
		if(!keys.get(Keys.FIRE))
			return Assets.getInstance().playerAnimations;
		else
			return Assets.getInstance().playerFireAnimations;
	}

	@Override
	public void visit(Door door) {
		if(cards.size() > 0) {
			door.open(cards.get(0));
			cards.remove(0);
		}
		super.visit(door);
	}

	@Override
	public void visit(DoorCard doorCard) {
		WorldController world = WorldController.getInstance();
		world.onCollectableTaken(doorCard);
		cards.add(doorCard);
	}

	
	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}


	@Override
	public void visit(PowerUp powerUp) {
		WorldController world = WorldController.getInstance();
		world.decoratePlayer(powerUp.decorate(this));
		world.onCollectableTaken(powerUp);
	}

	@Override
	public void visit(FinishBlock block) {
		super.visit(block);
		WorldController.getInstance().onLevelEnd();
	}

	public int getCardCount() {
		return cards.size();
	}
	
	public int getAmmoCount() {
		return weapon.getAmmoCount();
	}
	
	
}
