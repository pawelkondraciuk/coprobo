package pb.wi.coprobo.objects;

import pb.wi.coprobo.Assets;
import pb.wi.coprobo.visitor.Visitor;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class DestructableBlock extends StaticGameObject {

	TextureRegion texture;
	
	public enum BlockState { FULL, HALF, QUARTER, NONE };
	
	BlockState state = BlockState.FULL;
	
	public DestructableBlock(Vector2 position) {
		super(position);
		texture = Assets.getInstance().destructableBlockFull;
		bounds.add(new Rectangle(0, 0, 1, 1));
	}

	@Override
	public void render(SpriteBatch batch) {
		batch.draw(texture.getTexture(), position.x, position.y, origin.x, origin.y, dimension.x, dimension.y, scale.x, scale.y, rotation, texture.getRegionX(), texture.getRegionY(), texture.getRegionWidth(),
				texture.getRegionHeight(), false, false);
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}
	
	public void applyDamage() {
		if(state == BlockState.FULL) {
			texture = Assets.getInstance().destructableBlockHalf;
			state = BlockState.HALF;
		} else if (state == BlockState.HALF) {
			texture = Assets.getInstance().destructableBlockNone;
			state = BlockState.QUARTER;
		} else if(state == BlockState.QUARTER) {
			texture = Assets.getInstance().floor;
			state = BlockState.NONE;
			bounds.clear();
			bounds.add(new Rectangle(0, 0, 1, 0.2f));
		}
	}
}
