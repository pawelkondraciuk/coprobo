package pb.wi.coprobo.objects;

import pb.wi.coprobo.Assets;
import pb.wi.coprobo.objects.DestructableBlock.BlockState;
import pb.wi.coprobo.visitor.Visitor;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Door extends StaticGameObject {
	
	TextureRegion texture;
	
	public enum DoorState { OPEN, CLOSE };
	
	DoorState state = DoorState.CLOSE;
	
	public Door(Vector2 position) {
		super(position);
		texture = Assets.getInstance().closedDoor;
		bounds.add(new Rectangle(0, 0, 1, 1));
	}

	public void open(DoorCard card) {
		if(!card.getUsed() && state == DoorState.CLOSE) {
			state = DoorState.OPEN;
			bounds.clear();
			bounds.add(new Rectangle(0, 0, 1, 0.2f));
			texture = Assets.getInstance().openDoor;
		}
	}

	@Override
	public void render(SpriteBatch batch) {
		batch.draw(texture.getTexture(), position.x, position.y, origin.x, origin.y, dimension.x, dimension.y, scale.x, scale.y, rotation, texture.getRegionX(), texture.getRegionY(), texture.getRegionWidth(),
				texture.getRegionHeight(), false, false);
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}

	public DoorState getState() {
		return state;
	}
}
