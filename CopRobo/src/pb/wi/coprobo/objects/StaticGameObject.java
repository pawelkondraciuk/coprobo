package pb.wi.coprobo.objects;

import pb.wi.coprobo.visitor.Element;

import com.badlogic.gdx.math.Vector2;

public abstract class StaticGameObject extends GameObject implements Element {
	
	public StaticGameObject(Vector2 position) {
		super(position);
	}
}
