package pb.wi.coprobo.objects;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import pb.wi.coprobo.Assets;

import com.badlogic.gdx.math.Vector2;

public class Level implements Iterable<StaticGameObject> {

	private int width;
	private int height;
	
	private StaticGameObject[][] staticGameObjects;
	private List<StaticGameObject> collectable = new ArrayList<StaticGameObject>();
	List<pb.wi.coprobo.objects.Character> characterList = new ArrayList<pb.wi.coprobo.objects.Character>();
   
	private Vector2 startPosition = new Vector2();
	
	public StaticGameObject[][] getStaticGameObjects() {
		return staticGameObjects;
	}

	public void setStaticGameObjects(StaticGameObject[][] staticGameObjects) {
		this.staticGameObjects = staticGameObjects;
	}

	private Vector2 spanPosition;

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public StaticGameObject getStaticGameObject(int x, int y) {
		return staticGameObjects[x][y];
	}
	
	public void setStaticGameObject(StaticGameObject staticGameObject, int x, int y) {
		this.staticGameObjects[x][y] = staticGameObject;
	}

	public Level(int width, int height) {
		this.width = width;
		this.height = height;
		
		staticGameObjects = new StaticGameObject[width][height];
	}

    public Vector2 getSpanPosition() {
        return spanPosition;
    }

    public void setSpanPosition(Vector2 spanPosition) {
        this.spanPosition = spanPosition;
    }
    
    public void addCollectable(StaticGameObject staticGameObject) {
    	this.collectable.add(staticGameObject);
    }
    
    public List<StaticGameObject> getCollectable() {
    	return collectable;
    }
    
    public void addCharacter(Character character) {
    	characterList.add(character);
    }
    
    public List<pb.wi.coprobo.objects.Character> getCharacterList() {
    	return characterList;
    }
    
    public void setStartPosition(int x, int y) {
    	this.startPosition.x = x;
    	this.startPosition.y = y;
    }
    
    public Vector2 getStartPosition() {
    	return this.startPosition;
    }
    
    /*
     * Iterator
     */
    
    @Override
	public Iterator<StaticGameObject> iterator() {
		return new LevelIterator();
	}
    
    private class LevelIterator implements Iterator<StaticGameObject> {

		int i = 0;
		int j = 0;

		@Override
		public boolean hasNext() {
			return i < staticGameObjects.length && j < staticGameObjects[0].length;
		}

		@Override
		public StaticGameObject next() {
			StaticGameObject t = staticGameObjects[i][j];

			if (++j == staticGameObjects[0].length) {
				j = 0;
				i++;
			}

			return t;
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
	}
}
