package pb.wi.coprobo.objects;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import pb.wi.coprobo.Assets;
import pb.wi.coprobo.decorators.PlayerDecorator;
import pb.wi.coprobo.visitor.Visitor;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class PowerUp extends StaticGameObject {
	
	TextureRegion texture;
	Class<?> powerUpType;
	
	public PowerUp(Vector2 position, Class<?> powerUpType) {
		super(position);
		
		texture = Assets.getInstance().PowerUpCube;
		bounds.add(new Rectangle(0.15f, 0.25f, 0.6f, 0.6f));
		this.powerUpType = powerUpType;
	}

	@Override
	public void render(SpriteBatch batch) {
		batch.draw(texture.getTexture(), position.x, position.y, origin.x, origin.y, dimension.x, dimension.y, scale.x, scale.y, rotation, texture.getRegionX(), texture.getRegionY(), texture.getRegionWidth(),
				texture.getRegionHeight(), false, false);
	}
	
	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}
	
	public PlayerDecorator decorate(Player player) {
		Constructor<?> constructor;
		Object instance = null;
		try {
			constructor = powerUpType.getConstructor(Player.class);
			instance = constructor.newInstance(player);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return (PlayerDecorator) instance;
	}
}
