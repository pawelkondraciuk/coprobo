package pb.wi.coprobo.objects;

public interface Strategy {
	void findPlayer(Player player);
}
