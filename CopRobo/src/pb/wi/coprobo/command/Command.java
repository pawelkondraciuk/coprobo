package pb.wi.coprobo.command;

public interface Command {
	public void execute();
}
