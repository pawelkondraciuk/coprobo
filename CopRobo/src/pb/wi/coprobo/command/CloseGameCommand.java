package pb.wi.coprobo.command;

import com.badlogic.gdx.Gdx;

public class CloseGameCommand implements Command {
	
	public CloseGameCommand() {
		
	}
	
	@Override
	public void execute() {
		Gdx.app.exit();
	}
	
}
