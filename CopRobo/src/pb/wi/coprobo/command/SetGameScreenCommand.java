package pb.wi.coprobo.command;

import pb.wi.coprobo.CopRoboGame;
import pb.wi.coprobo.screens.GameScreen;

public class SetGameScreenCommand implements Command {
	
	CopRoboGame game;
	
	public SetGameScreenCommand(CopRoboGame game) {
		this.game = game;
	}
	
	@Override
	public void execute() {
		game.setScreen(new GameScreen(game));
	}
	
}
