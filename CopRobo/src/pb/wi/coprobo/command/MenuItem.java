package pb.wi.coprobo.command;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class MenuItem {
	private TextButton textButton;
	private Command command;
	
	public MenuItem(String content, float width, float height, Command command, Skin skin)
	{
		this.command = command;
		
		textButton = new TextButton(content, skin);
		textButton.setSize(width, height);
		textButton.addListener(new ClickListener() {
		    public void clicked(InputEvent event, float x, float y) {
		    	MenuItem.this.command.execute();
		    }
		});
	}
	
	public TextButton getTextButton() {
		return textButton;
	}
	
}
