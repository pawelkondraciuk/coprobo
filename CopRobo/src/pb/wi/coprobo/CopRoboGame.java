package pb.wi.coprobo;

import pb.wi.coprobo.screens.GameScreen;
import pb.wi.coprobo.screens.MainMenuScreen;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;

public class CopRoboGame extends Game {

	@Override
	public void create() {
		// Libgdx log level => Gdx.app.debug(TAG, "message");
		Gdx.app.setLogLevel(Application.LOG_DEBUG);
		// Set main screen
		setScreen(new MainMenuScreen(this));
	}
}
