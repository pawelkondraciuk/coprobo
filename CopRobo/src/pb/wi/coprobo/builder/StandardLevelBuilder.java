package pb.wi.coprobo.builder;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import pb.wi.coprobo.Assets;
import pb.wi.coprobo.decorators.DoubleDamage;
import pb.wi.coprobo.decorators.Immortal;
import pb.wi.coprobo.objects.Block;
import pb.wi.coprobo.objects.DestructableBlock;
import pb.wi.coprobo.objects.Door;
import pb.wi.coprobo.objects.DoorCard;
import pb.wi.coprobo.objects.FinishBlock;
import pb.wi.coprobo.objects.HardOpponent;
import pb.wi.coprobo.objects.Ladder;
import pb.wi.coprobo.objects.Ladder.LadderSegment;
import pb.wi.coprobo.objects.Level;
import pb.wi.coprobo.objects.NormalOpponent;
import pb.wi.coprobo.objects.PowerUp;
import pb.wi.coprobo.objects.TimeOfDay;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class StandardLevelBuilder implements LevelBuilder {

	private Level level = null;

	TextureRegion texture = null;
	List<Rectangle> bounds;
	Block block = null;
	TimeOfDay timeOfDay = new TimeOfDay();

	public StandardLevelBuilder() {

	}

	@Override
	public void createDoor(int x, int y) {
		texture = Assets.getInstance().openDoor;
		Door door = new Door(new Vector2(x, y));
		level.setStaticGameObject(door, x, y);
	}

	public void createFloor(int x, int y) {
		bounds = new ArrayList<Rectangle>();
		bounds.add(new Rectangle(0, 0, 1, 0.2f));
		texture = Assets.getInstance().floor;
		addBlock(x, y);
	}

	public void createWallLeft(int x, int y) {
		bounds = new ArrayList<Rectangle>();
		bounds.add(new Rectangle(0, 0, 0.2f, 1f));
		texture = Assets.getInstance().leftWall;
		addBlock(x, y);
	}

	public void createWallRight(int x, int y) {
		bounds = new ArrayList<Rectangle>();
		bounds.add(new Rectangle(0.8f, 0, 0.2f, 1f));
		texture = Assets.getInstance().rightWall;
		addBlock(x, y);
	}

	@Override
	public void createCornerLeft(int x, int y) {
		bounds = new ArrayList<Rectangle>();
		bounds.add(new Rectangle(0, 0, 1, 0.2f));
		bounds.add(new Rectangle(0, 0, 0.2f, 1f));
		texture = Assets.getInstance().leftCorner;
		addBlock(x, y);
	}

	@Override
	public void createCornerRight(int x, int y) {
		bounds = new ArrayList<Rectangle>();
		bounds.add(new Rectangle(0, 0, 1, 0.2f));
		bounds.add(new Rectangle(0.8f, 0, 0.2f, 1f));
		texture = Assets.getInstance().rightCorner;
		addBlock(x, y);
	}

	@Override
	public void createDestructableBlock(int x, int y) {
		DestructableBlock destructableBlock = new DestructableBlock(
				new Vector2(x, y));
		level.setStaticGameObject(destructableBlock, x, y);
	}
	

	@Override
	public void createBottomLadder(int x, int y) {
		Ladder ladder = new Ladder(new Vector2(x, y), LadderSegment.BOTTOM);
		level.setStaticGameObject(ladder, x, y);
	}

	@Override
	public void createMiddleLadder(int x, int y) {
		Ladder ladder = new Ladder(new Vector2(x, y), LadderSegment.MIDDLE);
		level.setStaticGameObject(ladder, x, y);
	}

	@Override
	public void createTopLadder(int x, int y) {
		Ladder ladder = new Ladder(new Vector2(x, y), LadderSegment.TOP);
		level.setStaticGameObject(ladder, x, y);
	}

	@Override
	public void createPowerUp(int x, int y) {
		
		Random randomizer = new Random();
		
		Vector2 position = new Vector2(x, y);
		PowerUp powerUp = null;
		
		switch(Math.round(randomizer.nextFloat()))
		{
		case 0:
			powerUp = new PowerUp(position, DoubleDamage.class);
			break;
		case 1:
			powerUp = new PowerUp(position, Immortal.class);
			break;
		}

		level.addCollectable(powerUp);
	}

	@Override
	public void createDoorCard(int x, int y) {
		DoorCard doorCard = new DoorCard(new Vector2(x, y));
		level.addCollectable(doorCard);
	}
	
	@Override
	public void createNormalOpponent(int x, int y) {
		NormalOpponent normalOpponent = new NormalOpponent(new Vector2(x, y + 0.5f), timeOfDay.isDay());
		level.addCharacter(normalOpponent);
	}

	@Override
	public void createHardOpponent(int x, int y) {
		HardOpponent hardOpponent = new HardOpponent(new Vector2(x, y + 0.5f), timeOfDay.isDay());
		level.addCharacter(hardOpponent);
	}
	
	public void createStartPosition(int x, int y) {
		level.setStartPosition(x, y);
	}

	@Override
	public void createNullBlock(int x, int y) {
		level.setStaticGameObject(null, x, y);
	}

	public Level getResult() {
		return level;
	}

	@Override
	public void initBuilder(int width, int height) {
		level = new Level(width, height);
	}

	private void addBlock(int x, int y) {
		block = new Block(new Vector2(x, y), texture, bounds);
		level.setStaticGameObject(block, x, y);
	}

	@Override
	public void createFinishBlock(int x, int y) {
		FinishBlock block = new FinishBlock(new Vector2(x, y));
		level.setStaticGameObject(block, x, y);
	}

	@Override
	public void createWallCenter(int x, int y) {
		bounds = new ArrayList<Rectangle>();
		bounds.add(new Rectangle(0.4f, 0, 0.2f, 1f));
		texture = Assets.getInstance().centerWall;
		addBlock(x, y);
	}

}
