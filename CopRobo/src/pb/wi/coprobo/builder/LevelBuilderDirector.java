package pb.wi.coprobo.builder;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import com.badlogic.gdx.files.FileHandle;

public class LevelBuilderDirector {

	LevelBuilder levelBuilder = null;

	public LevelBuilderDirector(LevelBuilder levelBuilder) {
		this.levelBuilder = levelBuilder;
	}

	public void setBuilder(LevelBuilder levelBuilder) {
		this.levelBuilder = levelBuilder;
	}

	public void createLevel(FileHandle levelFile) {
		int width = 0;
		int height = 0;

		char element;

		Scanner input = null;
		
		input = new Scanner(levelFile.readString());

		if (input.hasNext()) {
			width = Integer.valueOf(input.next());
		}

		if (input.hasNext()) {
			height = Integer.valueOf(input.next());
		}

		levelBuilder.initBuilder(width, height);

		for (int y = height - 1; y >= 0; y--) {
			for (int x = 0; x < width; x++) {

				if (input.hasNext()) {
					element = input.next().charAt(0);

					switch (element) {
					case '0':
						levelBuilder.createNullBlock(x, y);
						break;
					case '_':
						levelBuilder.createFloor(x, y);
						break;
					case '[':
						levelBuilder.createWallLeft(x, y);
						break;
					case '<':
						levelBuilder.createCornerLeft(x, y);
						break;
					case ']':
						levelBuilder.createWallRight(x, y);
						break;
					case '|':
						levelBuilder.createWallCenter(x, y);
						break;
					case '>':
						levelBuilder.createCornerRight(x, y);
						break;
					case 'D':
						levelBuilder.createDoor(x, y);
						break;
					case 'C':
						levelBuilder.createFloor(x, y);
						levelBuilder.createDoorCard(x, y);
						break;
					case '#':
						levelBuilder.createFloor(x, y);
						levelBuilder.createDestructableBlock(x, y);
						break;
					case 'P':
						levelBuilder.createFloor(x, y);
						levelBuilder.createPowerUp(x, y);
						break;
					case 'H':
						levelBuilder.createFloor(x, y);
						levelBuilder.createHardOpponent(x, y);
						break;
					case 'N':
						levelBuilder.createFloor(x, y);
						levelBuilder.createNormalOpponent(x, y);
						break;
					case 'S':
						levelBuilder.createFloor(x, y);
						levelBuilder.createStartPosition(x, y);
						break;
					case '{':
						levelBuilder.createTopLadder(x, y);
						break;
					case '=':
						levelBuilder.createMiddleLadder(x, y);
						break;
					case '}':
						levelBuilder.createBottomLadder(x, y);
						break;
					case 'F':
						levelBuilder.createFinishBlock(x, y);
						break;
					}
				}

			}
		}
	}

}
