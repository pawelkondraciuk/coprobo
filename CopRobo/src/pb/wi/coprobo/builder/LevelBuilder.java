package pb.wi.coprobo.builder;


public interface LevelBuilder {
	public void initBuilder(int width, int height);
	public void createDoor(int x, int y);
	public void createFloor(int x, int y);
	public void createWallLeft(int x, int y);
	public void createWallRight(int x, int y);
	public void createCornerLeft(int x, int y);
	public void createCornerRight(int x, int y);
	public void createDestructableBlock(int x, int y);
	public void createPowerUp(int x, int y);
	public void createBottomLadder(int x, int y);
	public void createMiddleLadder(int x, int y);
	public void createTopLadder(int x, int y);
	public void createDoorCard(int x, int y);
	public void createNullBlock(int x, int y);
	public void createNormalOpponent(int x, int y);
	public void createHardOpponent(int x, int y);
	public void createStartPosition(int x, int y);
	public void createFinishBlock(int x, int y);
	public void createWallCenter(int x, int y);
}
