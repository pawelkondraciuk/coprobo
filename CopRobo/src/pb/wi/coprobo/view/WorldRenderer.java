package pb.wi.coprobo.view;

import pb.wi.coprobo.Assets;
import pb.wi.coprobo.CameraHelper;
import pb.wi.coprobo.CameraHelper.ViewportMode;
import pb.wi.coprobo.controller.WorldController;
import pb.wi.coprobo.objects.Bullet;
import pb.wi.coprobo.objects.DamageAnimation;
import pb.wi.coprobo.objects.Explosion;
import pb.wi.coprobo.objects.GameObject;
import pb.wi.coprobo.objects.StaticGameObject;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.sun.xml.internal.fastinfoset.algorithm.BuiltInEncodingAlgorithm.WordListener;

public class WorldRenderer implements Disposable {
	private OrthographicCamera camera;
	private SpriteBatch worldBatch;
	private SpriteBatch hudBatch;
	private ShapeRenderer shapeRenderer;
	private WorldController worldController;
	public static ShaderProgram shaderMonochrome;
	
	// HP BAR
	private Rectangle healthBar = new Rectangle(20, 35, 200, 20);

	BitmapFont font;

	public WorldRenderer(WorldController worldController) {
		this.worldController = worldController;
		init();
	}
	
	private void init() {
		camera = CameraHelper.createCamera2(ViewportMode.PIXEL_PERFECT, Assets.VIRTUAL_WIDTH, Assets.VIRTUAL_HEIGHT, Assets.PIXEL_DENSITY);
		
		worldBatch = new SpriteBatch();
		worldBatch.setProjectionMatrix(camera.combined);
		
		hudBatch = new SpriteBatch();
		shapeRenderer = new ShapeRenderer();
		
		font = new BitmapFont();

		shaderMonochrome = new ShaderProgram(Gdx.files.internal("shaders//monochrome.vs"), Gdx.files.internal("shaders//monochrome.fs"));
		if (!shaderMonochrome.isCompiled()) {
			String msg = "Could not compile shader program: " + shaderMonochrome.getLog();
			throw new GdxRuntimeException(msg);
		}
	}
	
	public void resize(int width, int height) {
		camera = CameraHelper.createCamera(false, Assets.VIRTUAL_WIDTH, Assets.VIRTUAL_HEIGHT);//CameraHelper.createCamera2(ViewportMode.STRETCH_TO_ASPECT, Assets.VIRTUAL_WIDTH, Assets.VIRTUAL_HEIGHT, Gdx.graphics.getDensity());
		camera.update();
	}
	
	public void render() {
		worldController.getCameraHelper().applyTo(camera);
		worldBatch.setProjectionMatrix(camera.combined);
		
		worldBatch.begin();
			for(StaticGameObject obj : worldController.getCurrentLevel()) {
				if(obj != null)
					obj.render(worldBatch);
			}
			
			
			for(Bullet b : worldController.getBullets()) {
				b.render(worldBatch);
			}
			
			for(Explosion exp : worldController.getExplosions()) {
				exp.render(worldBatch);
			}
			
			for(StaticGameObject obj : worldController.getCollectable()) {
				obj.render(worldBatch);
			}
			
			for(GameObject obj : worldController.getCharacterList()) {
				obj.render(worldBatch);
			}
		worldBatch.end();
		
		hudBatch.begin();
			font.draw(hudBatch, "fps: " + Gdx.graphics.getFramesPerSecond(), 0, Gdx.graphics.getHeight());
			font.draw(hudBatch, "Poziom: " + worldController.getCurLevel(), 10, 40);
			font.draw(hudBatch, "HP: " + worldController.getPlayer().getHealth(), 120, 40);
			font.draw(hudBatch, (int)worldController.getPlayer().getPercentage() + "%", 360, 40); // Po ustaleniu max hp itd. mo�na obliczenie przenie�� do kontrolera �eby nie oblicza� za ka�dym razem
			font.draw(hudBatch, "Amunicja: " + worldController.getPlayer().getAmmoCount(), 420, 40);
			font.draw(hudBatch, "Karty: " + worldController.getPlayer().getCardCount(), 535, 40);
			if(worldController.hasPowerUp()) {
				font.draw(hudBatch, worldController.getPlayer().toString() + ": " + (int)worldController.getPowerUpTime(), 610, 40);
			}
			
			for (DamageAnimation d : worldController.getDamageAnimations()) {
				Vector3 coords = new Vector3(d.getX(), d.getY(), 0);
				camera.project(coords); // zamiana jednostek 
				font.draw(hudBatch, Integer.toString(d.getDamage()), coords.x + 32.0f, coords.y + 64.0f);
			}
			
		hudBatch.end();
		
		shapeRenderer.begin(ShapeType.Filled);
			shapeRenderer.setColor(Color.RED);
			shapeRenderer.rect(150, 24, 2 * worldController.getPlayer().getPercentage(), 20);
		shapeRenderer.end();
	}
	
	@Override
	public void dispose() {
		// clean up
		worldBatch.dispose();
	}
	
}
